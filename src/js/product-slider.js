/* Product Slider */
$(document).ready(function(){
    let $productSlider = $('[data-slider="product"]');

    $productSlider.slick({
        infinite: false,
        arrows: false,
        asNavFor: '[data-slider="product-thumbs"]',
        responsive: [
            {
                breakpoint: bpLG,
                settings: {
                    dots: true,
                }
            },
        ]
    });

    let $productThumbs = $('[data-slider="product-thumbs"]');
    $productThumbs.slick({
        infinite: false,
        vertical: true,
        verticalSwiping: true,
        slidesToShow: 6,
        slidesToScroll: 1,
        focusOnSelect: true,
        asNavFor: '[data-slider="product"]',
        responsive: [
            {
                breakpoint: bpLG,
                settings: 'unslick'
            },
        ]
    });

    $(window).on('orientationchange resize', function() {
        $productSlider.slick('resize');
        $productThumbs.slick('resize');
    });
});
