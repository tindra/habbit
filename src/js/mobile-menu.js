/* Mobile menu */
let $mobileMenu = $('[data-component="mobile-menu"]');
let $mobileMenuTrigger = $('[data-element="mobile-menu-trigger"]');
let $mobileMenuToggle = $('[data-element="mobile-menu-toggle"]');
let $mobileMenuClose = $('[data-element="mobile-menu-close"]');

function hideMobileMenu(component) {
    component.removeClass(visibleClass);

    let parentMenu = component.parents('[data-component="mobile-menu"]');

    if (parentMenu.length === 0) {
        $body.removeClass(mobileMenuVisibleClass);
        $body.removeClass(lockedScrollClass);
    }
}

function showMobileMenu(component) {
    if ($(window).width() < bpXL) {
        component.addClass(visibleClass);
        $body.addClass(lockedScrollClass);
        $body.addClass(mobileMenuVisibleClass);
    }
}

function desktopCheck() {
    if ($(window).width() >= bpXL) {
        hideMobileMenu($mobileMenu);
    }
}

$(document).ready(function(){

    $mobileMenuToggle.on('click', function(e) {
        e.preventDefault();

        let $toggle = $(this);
        let toggleSrc = $toggle.attr('data-src');
        let $component = $('[data-component="mobile-menu"][data-src="'+ toggleSrc +'"]');

        if ($component.hasClass(visibleClass)) {
            hideMobileMenu($component);
            $('[data-component="mobile-menu"]').removeClass(visibleClass);
        } else {
            $('[data-component="mobile-menu"]').not($component).removeClass(visibleClass);
            showMobileMenu($component);
        }
    });

    $mobileMenuTrigger.on('click', function(e) {
        e.preventDefault();

        let $trigger = $(this);
        let triggerSrc = $trigger.attr('data-src');
        let $component = $('[data-component="mobile-menu"][data-src="'+ triggerSrc +'"]');

        showMobileMenu($component);
    });

    $mobileMenuClose.on('click', function(e) {
        e.preventDefault();

        let $closeButton = $(this);
        let $component = $closeButton.closest('[data-component="mobile-menu"]');

        hideMobileMenu($component);
    });

    $(document).on('keyup', function(e) {
        if ($(window).width() < bpXL) {
            if (e.key === "Escape" || e.keyCode === 27) {
                hideMobileMenu($mobileMenu);
            }
        }
    });

    $(document).on('click', function(e) {
        if ($(window).width() < bpXL) {
            if ($mobileMenu.has(e.target).length === 0 && !$mobileMenu.is(e.target) && $mobileMenuTrigger.has(e.target).length === 0 && !$mobileMenuTrigger.is(e.target) && $mobileMenuToggle.has(e.target).length === 0 && !$mobileMenuToggle.is(e.target)) {
                hideMobileMenu($mobileMenu);
            }
        }
    });

    desktopCheck();

    $(window).on('orientationchange resize', function() {
        desktopCheck();
    });
});