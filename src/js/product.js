/* Orderbar on scroll */
document.addEventListener('DOMContentLoaded', function() {
    const $orderbarClone = document.querySelector('[data-component="orderbar-clone"]');

    if (!$orderbarClone) {
        return false;
    }

    const $orderbar = document.querySelector('[data-component="orderbar"]');
    const navbarHeight = document.querySelector('[data-component="navbar"]').offsetHeight;
    const orderbarOffset = $orderbar.getBoundingClientRect().top + $orderbar.offsetHeight - navbarHeight;

    function orderbarOnScroll() {
        let onScroll = function () {
            $orderbarClone.classList.toggle(visibleClass, window.scrollY > orderbarOffset);
        };

        onScroll();
        window.addEventListener('scroll', function() {
            onScroll();
        });
    }

    orderbarOnScroll();
});