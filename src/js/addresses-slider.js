/* Addresses slider */
$(document).ready(function(){
    let $addressesListSlider = $('[data-slider="addresses-list"]');
    let $addressesSlider = $('[data-slider="addresses"]');

    $addressesListSlider.slick({
        mobileFirst: true,
        infinite: false,
        slidesToShow: 1,
        variableWidth: true,
        autoSlidesToShow: true,
        swipeToSlide: true,
        arrows: false,
        responsive: [
            {
                breakpoint: bpLG - 1,
                settings: "unslick"
            },
        ]
    });

    $addressesSlider.slick({
        infinite: false,
        slidesToShow: 1,
        variableWidth: true,
        autoSlidesToShow: true,
        swipeToSlide: true,
        arrows: false,
    });

    $(window).on('orientationchange resize', function() {
        $addressesListSlider.slick('resize');
        $addressesSlider.slick('resize');
    });
});