/* Show more */
$(document).ready(function() {
    let $showMoreComponent = $('[data-component="show-more"]');

    function showMore() {
        $showMoreComponent.each(function(index, component) {
            let $component = $(component);
            let $list = $component.find('[data-element="show-more-list"]');
            let $items = $component.find('[data-element="show-more-item"]');
            let $button = $component.find('[data-element="show-more-button"]');
            let $pagination = $component.find('[data-element="show-more-pagination"]');
            let buttonElement = '';

            if ($(window).width() < bpLG) {
                let itemsLength = $items.length;
                let itemsNumber = 4;
                let loadItemsNumber = itemsNumber;
                let itemsNumberDataset = $component.attr('data-items');
                let buttonCount = $component.attr('data-button-count');

                if (itemsNumberDataset) {
                    itemsNumber = itemsNumberDataset;
                }

                loadItemsNumber = itemsNumber;

                if (buttonCount) {
                    buttonElement = '<div class="pagination" data-element="show-more-pagination"><button class="button button--link button--link-light" data-element="show-more-button">Show <span data-element="show-more-button-count">&nbsp;'+loadItemsNumber+'&nbsp;</span> more</button></div>';
                } else {
                    buttonElement = '<div class="pagination" data-element="show-more-pagination"><button class="button button--link button--link-light" data-element="show-more-button">Show more</button></div>';
                }

                if (itemsLength > itemsNumber) {
                    if ($pagination.length === 0) {
                        $list.after(buttonElement);
                    }

                    $items.slice(loadItemsNumber).prop('hidden', true);
                } else {
                    $pagination.remove();
                }

            } else {
                $items.prop('hidden', false);
                $pagination.remove();
            }

        });
    }

    $(window).on('orientationchange resize load', function() {
        showMore();
    });
});