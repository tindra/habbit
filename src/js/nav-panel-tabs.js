/* Panel on mobile, tabs on desktop */
$(document).ready(function(){
    responsivePanelTabs();

    $(window).on('orientationchange resize', function() {
        responsivePanelTabs();
    });

    function responsivePanelTabs() {
        let $panelTabs = $('[data-component="panel-tabs"]');
        let $panelTabsToggle = $('[data-element="panel-tabs-toggle"]');
        let $panelTabsContent = $('[data-element="panel-tabs-content"]');

        if ($(window).width() < bpXL) {
            $panelTabs.each(function (index, item) {
                let $component = $(item);
                let $tab = $component.find('[data-element="panel-tabs-tab"]');

                $tab.hide().removeClass(activeClass);
                $component.find('[data-element="panel-tabs-toggle"]').removeClass(activeClass);
                $component.find('[data-element="panel-tabs-item"]').addClass(collapsedClass);

                $tab.each(function (index, item) {
                    let $tab = $(item);
                    let tabID = $tab.attr('data-id');
                    let $toggle = $component.find('[data-element="panel-tabs-toggle"][data-id="'+tabID+'"]');
                    let $item = $toggle.closest('[data-element="panel-tabs-item"][data-id="'+tabID+'"]');

                    $toggle.after($tab);
                });
            });

            $panelTabs.on('click', '[data-element="panel-tabs-toggle"]:not([data-link="normal"])', function(e) {
                e.preventDefault();

                let $toggle = $(e.currentTarget);
                let $item   = $toggle.closest('[data-element="panel-tabs-item"]');
                let $tab    = $item.find('[data-element="panel-tabs-tab"]');

                if ($item.hasClass(collapsedClass)) {
                    $tab.slideDown(250);
                    $item.removeClass(collapsedClass);
                } else {
                    $tab.slideUp(250);
                    $item.addClass(collapsedClass);
                }
            });
        } else {
            $panelTabs.each(function (index, item) {
                let $component = $(item);
                let $tab = $component.find('[data-element="panel-tabs-tab"]');
                let $content = $component.find('[data-element="panel-tabs-content"]');

                $tab.attr('style', '');
                $component.find('[data-element="panel-tabs-item"]').removeClass(collapsedClass);

                $tab.appendTo($content);
            });

            $panelTabsToggle.on('mouseenter', function(e) {
                let $toggle = $(this);
                let toggleID = $toggle.attr('data-id');
                let $tabActive = '[data-element="panel-tabs-tab"][data-id="' + toggleID +'"]';
                let $component = $toggle.closest('[data-component="panel-tabs"]');
                let $defaultImg = $component.find('[data-element="panel-tabs-default-img"]');

                console.log($defaultImg);

                if (!($toggle.hasClass(activeClass))) {
                    $defaultImg.removeClass(activeClass);
                    $component.find('[data-element="panel-tabs-tab"]').removeClass(activeClass);
                    $component.find('[data-element="panel-tabs-toggle"]').removeClass(activeClass);
                    $component.find($tabActive).addClass(activeClass);
                    $toggle.addClass(activeClass);
                }
            });

            $panelTabsContent.on('mouseleave', function(e) {
                let $component = $(this).closest('[data-component="panel-tabs"]');
                let $defaultImg = $component.find('[data-element="panel-tabs-default-img"]');

                $component.find('[data-element="panel-tabs-tab"]').removeClass(activeClass);
                $component.find('[data-element="panel-tabs-toggle"]').removeClass(activeClass);
                $defaultImg.addClass(activeClass);
            });
        }
    }
});