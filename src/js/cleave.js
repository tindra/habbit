/* Cleave inputs */
(function() {
    let $codeInputs = document.querySelectorAll('[data-element="code-mask"]');

    if ($codeInputs.length !== 0) {
        for (const inputCode of $codeInputs) {
            let codeCleave = new Cleave(inputCode, {
                numericOnly: true,
                blocks: [5],
            });
        }
    }

    let $phoneInputs = document.querySelectorAll('[data-element="phone-mask"]');

    if ($phoneInputs.length !== 0) {
        for (const inputPhone of $phoneInputs) {
            let phoneCleave = new Cleave(inputPhone, {
                delimiters: ['+91 ', ' ', ' ', ' ', ' '],
                blocks: [0, 3, 3, 2, 2],
                numericOnly: true
            });
        }
    }
})();