/* Dropdown */

$(document).ready(function(){
    $('[data-element="dropdown-toggle"]').on('click', function(e){
        e.preventDefault();
    });

    $('[data-type="default"] [data-element="dropdown-toggle"]').each(function () {
        let tippyDropdown = tippy(this, {
            content(reference) {
                const id = reference.getAttribute('data-template');
                const template = document.getElementById(id);
                return template.innerHTML;
            },
            allowHTML: true,
            arrow: false,
            theme: 'light',
            trigger: 'click',
            placement: 'bottom-start',
            interactive: true,
            appendTo: document.body,
            popperOptions: {
                positionFixed: true,
            },
            zIndex: 99,
            offset: [0, 4],
            onShown: function(instance) {
                let $dropdownOption = $(instance.popper).find('[data-element="dropdown-option"]');
                $dropdownOption.on('click', function(e){
                    e.preventDefault();

                    let $option = $(this);
                    let optionText = $option.html();
                    let optionsParentId = $option.closest('[data-element="dropdown-content"]').attr('data-id');
                    let $dropdown = $('[data-template="'+optionsParentId+'"]').closest('[data-component="dropdown"]');
                    let $toggleText = $dropdown.find('[data-element="dropdown-toggle-text"]');

                    $option.siblings().removeClass(selectedClass);
                    $option.addClass(selectedClass);
                    $toggleText.html(optionText);

                    instance.hide();
                });
            }
        });
    });

    $('[data-type="multiple"] [data-element="dropdown-toggle"]').each(function () {
        let tippyDropdown = tippy(this, {
            content(reference) {
                const id = reference.getAttribute('data-template');
                const template = document.getElementById(id);
                return template.innerHTML;
            },
            allowHTML: true,
            arrow: false,
            theme: 'light',
            trigger: 'click',
            placement: 'bottom-start',
            interactive: true,
            appendTo: 'parent',
            onShown: function(instance) {
                let $dropdownCheckbox = $(instance.popper).find('[data-element="dropdown-checkbox"]');
                let $dropdownCheckboxAll = $(instance.popper).find('[data-element="dropdown-checkbox-all"]');

                $dropdownCheckboxAll.on('change', function(e){

                    let $checkboxAll = $(this);
                    let $dropdown = $checkboxAll.closest('[data-component="dropdown"]');
                    let $toggleAmount = $dropdown.find('[data-element="dropdown-toggle-amount"]');
                    let checkedItems = $dropdown.find('input[type=checkbox]:checked:not([data-element="dropdown-checkbox-all"])');

                    if ($checkboxAll.prop('checked')) {
                        checkedItems.prop('checked', false).change();
                        $toggleAmount.html('');
                    }
                });

                $dropdownCheckbox.on('change', function(e){

                    let $checkbox = $(this);
                    let $dropdown = $checkbox.closest('[data-component="dropdown"]');
                    let $checkboxAll = $dropdown.find('[data-element="dropdown-checkbox-all"]');
                    let $toggleAmount = $dropdown.find('[data-element="dropdown-toggle-amount"]');
                    let toggleAmountValue = +$toggleAmount.html();
                    let checkedItems = $dropdown.find('[data-element="dropdown-checkbox"]:checked').length;

                    if (checkedItems > 0) {
                        $checkboxAll.prop('checked', false).change();
                        $toggleAmount.html(checkedItems);
                    } else {
                        $checkboxAll.prop('checked', true).change();
                        $toggleAmount.html('');
                    }
                });
            }
        });
    });
});