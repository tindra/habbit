/* Hero slider */
$(document).ready(function(){
    let $heroSlider = $('[data-slider="hero"]');

    $heroSlider.slick({
        dots: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 5000,
    });

    heroContentAdjustment();
    $(window).on('resize orientationchange', function() {
        $heroSlider.slick('resize');
        heroContentAdjustment();
    });

    function heroContentAdjustment() {
        let $heroContent = $('[data-element="hero-content"]');

        $heroContent.css('height', '');

        let tallestContent = Math.max.apply(Math, $heroContent.map(function(index, el) {
            return $(el)[0].scrollHeight;
        }));

        if ($(window).width() < bpLG) {
            $heroContent.css('height', tallestContent + 'px');
        } else {
            $heroContent.css('height', '');
        }
    };
});