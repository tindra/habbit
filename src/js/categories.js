/* Categories on scroll */
document.addEventListener('DOMContentLoaded', function() {
    const $categories = document.querySelector('[data-component="categories"]');

    if (!$categories) {
        return false;
    }
    const $navbar = document.querySelector('[data-component="navbar"]');
    const $categoriesLinks = $categories.querySelectorAll('a[href^="#"][data-scroll]');
    const mainOffset = parseInt(window.getComputedStyle($main[0], null).getPropertyValue('padding-top')) - 40;

    let onScroll = function () {
        $categories.classList.toggle(fixedClass, window.scrollY > 0);
    };

    let scrollSpy = function () {
        if ($categoriesLinks.length === 0) {
            return false;
        }

        let scrollPos = (window.scrollY || document.documentElement.scrollTop) + mainOffset;

        Array.from($categoriesLinks).forEach((link) => {
            const $elem = document.querySelector(link.getAttribute('href'));

            if (!$elem) {
                return false;
            }

            return $elem.offsetTop <= scrollPos && ($elem.offsetTop + $elem.clientHeight) > scrollPos
            ? link.classList.add(activeClass)
            : link.classList.remove(activeClass);
        });

        if (scrollPos === mainOffset) {
            $categories.querySelector('a.'+activeClass+'[href^="#"][data-scroll]').classList.remove(activeClass)
            $categoriesLinks[0].classList.add(activeClass)
        }
    };

    onScroll();
    scrollSpy();
    window.addEventListener('scroll', function() {
        onScroll();
        scrollSpy();
    });
});