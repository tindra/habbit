/* Options */
$(document).ready(function(){
    $('[data-element="options-item"]').on('click', function(e){
        let $option = $(this);
        let $component = $option.closest('[data-component="options"]');
        let $data = $component.find('[data-element="options-data"]');
        let $product = $('[data-component="product"]');
        let optionTitle = $option.attr('title');

        $data.html(optionTitle);

        if ($option.hasClass(disabledClass)) {
            $product.addClass(disabledClass);
        } else {
            $product.removeClass(disabledClass);
        }
    });
});