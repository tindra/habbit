/* Tippy */
let tooltip = tippy('[data-element="tooltip-trigger"]', {
    placement: 'top-start',
    maxWidth: 153,
    theme: 'light',
    offset: [0, 8],
    arrow: false,
    allowHTML: true,
    zIndex: 99,
    content(reference) {
        let tooltipContent = '<div class="tooltip">'+ reference.dataset.content + '</div>'
        return tooltipContent;
    },
});

let popover = tippy('[data-element="popover-trigger"]', {
    maxWidth: 333,
    theme: 'light',
    offset: [0, 21],
    allowHTML: true,
    interactive: true,
    zIndex: 99,
});

let productPopover = tippy('[data-element="product-popover-trigger"]', {
    maxWidth: 282,
    theme: 'light',
    offset: [0, 5],
    allowHTML: true,
    arrow: false,
    interactive: true,
    appendTo: () => document.body,
    interactive: true,
    zIndex: 99,
    content(reference) {
        const id = reference.getAttribute('data-template');
        const template = document.getElementById(id);
        return template.innerHTML;
    },
});

window.addEventListener('scroll', function() {
    tippy.hideAll();
});