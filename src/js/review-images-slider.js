/* Review images Slider */
$(document).ready(function(){
    let $reviewImagesSlider = $('[data-slider="review-images"]');

    $reviewImagesSlider.each(function() {
        var $gallery = $(this);

        $gallery
        .slick({
            mobileFirst: true,
            infinite: false,
            arrows: false,
            swipeToSlide: true,
            slidesToShow: 1,
            variableWidth: true,
            autoSlidesToShow: true,
            responsive: [
                {
                    breakpoint: bpLG - 1,
                    settings: 'unslick'
                },
            ]
        })
        .magnificPopup({
            type: 'image',
            closeOnContentClick: false,
            fixedContentPos: false,
            mainClass: 'mfp-zoom-in',
            delegate: '[data-element="gallery-trigger"]:not(.slick-cloned)',
            image: {
                verticalFit: true,
            },
            gallery: {
                enabled: true
            },
            callbacks: {
                open: function() {
                    $('.body').css('overflow', "hidden").addClass(modalVisibleClass);
                    var current = $gallery.slick('slickCurrentSlide');
                    $gallery.magnificPopup('goTo', current);
                },
                close: function() {
                    $('.body').css('overflow', "").removeClass(modalVisibleClass);
                },
                beforeClose: function() {
                    $gallery.slick('slickGoTo', parseInt(this.index));
                },
                buildControls: function() {
                    // re-appends controls inside the main container
                    this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
                }
            }
        });

        reviewsImagesTotal($gallery);
        $(window).on('orientationchange resize', function() {
            $gallery.slick('resize');
            reviewsImagesTotal($gallery);
        });
    });

    function reviewsImagesTotal(slider) {
        if ($(window).width() >= bpLG) {
            let $slider = $(slider)
            let $sliderImages = $slider.find('[data-element="gallery-trigger"]');
            let sliderImagesCount = $sliderImages.length;
            let sliderImagesVisivbleCount = 5;
            let sliderImagesMoreCount = sliderImagesCount - sliderImagesVisivbleCount;

            if (sliderImagesMoreCount > 0) {
                $slider.children('[data-element="gallery-trigger"]').eq(sliderImagesVisivbleCount - 1).attr('data-title', '+'+sliderImagesMoreCount);
            }
        }
    };

});