/* Partners slider */
$(document).ready(function(){
    let $partnersSlider = $('[data-slider="partners"]');

    $partnersSlider.slick({
        infinite: false,
        slidesToShow: 4,
        swipeToSlide: true,
        responsive: [
            {
                breakpoint: bpLG,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: bpSM,
                settings: {
                    slidesToShow: 2,
                }
            },
        ]
    });

    $(window).on('orientationchange resize', function() {
        $partnersSlider.slick('resize');
    });
});