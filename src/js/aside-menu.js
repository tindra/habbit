/* Mobile menu */
let $asideMenu = $('[data-component="aside-menu"]');
let $asideMenuTrigger = $('[data-element="aside-menu-trigger"]');
let $asideMenuClose = $('[data-element="aside-menu-close"]');

function hideasideMenu(component) {
    component.removeClass(visibleClass);
    $body.removeClass(asideMenuVisibleClass);
    $body.removeClass(lockedScrollClass);
}

function showasideMenu(component) {
    component.addClass(visibleClass);
    $body.addClass(lockedScrollClass);
    $body.addClass(asideMenuVisibleClass);
}

$(document).ready(function(){

    $asideMenuTrigger.on('click', function(e) {
        e.preventDefault();

        let $trigger = $(this);
        let triggerSrc = $trigger.attr('data-src');
        let $component = $('[data-component="aside-menu"][data-src="'+ triggerSrc +'"]');

        showasideMenu($component);
    });

    $asideMenuClose.on('click', function(e) {
        e.preventDefault();

        let $closeButton = $(this);
        let $component = $closeButton.closest('[data-component="aside-menu"]');

        hideasideMenu($component);
    });

    $(document).on('keyup', function(e) {
        if (e.key === "Escape" || e.keyCode === 27) {
            hideasideMenu($asideMenu);
        }
    });

    $(document).on('click', function(e) {
        if ($asideMenu.has(e.target).length === 0 && !$asideMenu.is(e.target) && $asideMenuTrigger.has(e.target).length === 0 && !$asideMenuTrigger.is(e.target)) {
            hideasideMenu($asideMenu);
        }
    });
});