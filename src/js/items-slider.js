/* Items slider */
$(document).ready(function(){
    let $itemsSlider = $('[data-slider="items"]');

    $itemsSlider.each(function(index, slider) {
        let $slider = $(slider);

        let $prevButton = $slider.closest('.section').find('.section-slider__prev');
        let $nextButton = $slider.closest('.section').find('.section-slider__next');

        $slider.slick({
            infinite: false,
            slidesToShow: 4,
            prevArrow: $prevButton,
            nextArrow: $nextButton,
            swipeToSlide: true,
            responsive: [
                {
                    breakpoint: bpXL,
                    settings: {
                        slidesToShow: 1,
                        variableWidth: true,
                        autoSlidesToShow: true,
                        dots: true
                    }
                },
                {
                    breakpoint: bpMD,
                    settings: {
                        slidesToShow: 1,
                        variableWidth: true,
                        autoSlidesToShow: true,
                        dots: true
                    }
                },
            ]
        });

        $(window).on('orientationchange resize', function() {
            $slider.slick('resize');
        });
    });

});