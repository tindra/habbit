/* Catalog slider */
$(document).ready(function(){
    let $catalogSlider = $('[data-slider="catalog"]');
    let $catalogSliderDefault = $('[data-slider="catalog-default"]');

    $catalogSlider.each(function(index, slider) {
        let $slider = $(slider);

        let $prevButton = $slider.closest('.section').find('.section-slider__prev');
        let $nextButton = $slider.closest('.section').find('.section-slider__next');

        $slider.slick({
            infinite: false,
            slidesToShow: 4,
            swipeToSlide: true,
            prevArrow: $prevButton,
            nextArrow: $nextButton,
            responsive: [
                {
                    breakpoint: bpXL,
                    settings: {
                        slidesToShow: 1,
                        variableWidth: true,
                        autoSlidesToShow: true,
                        dots: true
                    }
                },
                {
                    breakpoint: bpLG,
                    settings: 'unslick'
                },
            ]
        });

        $(window).on('orientationchange resize', function() {
            $slider.slick('resize');
        });
    });

    $catalogSliderDefault.each(function(index, slider) {
        let $slider = $(slider);

        let $prevButton = $slider.closest('.section').find('.section-slider__prev');
        let $nextButton = $slider.closest('.section').find('.section-slider__next');

        $slider.slick({
            infinite: false,
            slidesToShow: 4,
            prevArrow: $prevButton,
            nextArrow: $nextButton,
            swipeToSlide: true,
            responsive: [
                {
                    breakpoint: bpXL,
                    settings: {
                        slidesToShow: 3,
                        dots: true
                    }
                },
                {
                    breakpoint: bpLG,
                    settings: {
                        slidesToShow: 2,
                        variableWidth: true,
                        autoSlidesToShow: true,
                        dots: true
                    }
                },
            ]
        });

        $(window).on('orientationchange resize', function() {
            $slider.slick('resize');
        });
    });

});