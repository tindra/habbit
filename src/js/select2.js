/* Select2 */
$(document).ready(function() {
    let selectCommonOptions = {
        width: '100%',
    };

    let $select = $('[data-element="select"]');

    if (!$select) {
        return false;
    }

    $select.each(function(index, select) {
        let $selectEl = $(select);

        $selectEl.select2(selectCommonOptions).on('select2:open', function(e){
            $('.select2-search__field').attr('placeholder', $(this).attr('data-search-placeholder'));
        });
    });
});