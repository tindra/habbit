/* Animate on Scroll */
$(document).ready(function(){
    AOS.init({
        duration: 500,
        easing: 'ease-in-out',
    });
});