let scroll = new SmoothScroll('[data-scroll]', {
    speed: 500,
    easing: 'easeInOutCubic',
    header: '[data-component="navbar"]',
    offset: function (anchor, toggle) {
        if (toggle.closest('[data-component="categories"]')) {
            let categoriesHeight = document.querySelector('[data-component="categories"]').offsetHeight;
            return categoriesHeight;
        } else {
            return 100;
        }
    }
});