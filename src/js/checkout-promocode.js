/* Promocode in checkout */
$(document).ready(function(){
    let $bonusControl     = $('[data-component="bonus"] input[name="bonus"]:radio');
    let $bonusPromocode   = $('[data-element="bonus-promocode"]');
    let $bonusPoints      = $('[data-element="bonus-points"]');
    let $promocodeControl = $('[data-element="promocode-control"]');
    let $promocodeTag     = $('[data-element="promocode-tag"]');
    let $promocodeName    = $('[data-element="promocode-name"]');
    let $promocodeField   = $('[data-element="promocode-field"]');
    let $promocodeApply   = $('[data-element="promocode-apply"]');
    let $promocodeRow     = $('[data-element="promocode-row"]');
    let $pointsControl    = $('[data-element="points-control"]');
    let $pointsRow        = $('[data-element="points-row"]');
    let $pointsData       = $('[data-element="points-data"]');
    let $pointsApply      = $('[data-element="points-apply"]');

    $promocodeTag.on('click', function(e) {
        e.preventDefault();

        let $tag = $(this);

        $promocodeControl.prop('checked', false).change();
        $bonusPromocode.removeClass(selectedClass).addClass(readyClass);
        $promocodeRow.prop('hidden', true);
        $promocodeName.prop('hidden', true);
        $promocodeField.prop('hidden', false);
    });

    $bonusPromocode.on('click', function(e) {
        let $item = $(this);

        if (!$item.hasClass(selectedClass)) {
            $item.addClass(selectedClass);
            $bonusPoints.removeClass(selectedClass);
            $promocodeControl.prop('checked', true).change();
            $pointsControl.prop('checked', false).change();

            $pointsRow.prop('hidden', true);

            if (!$promocodeField.prop('hidden')) {
                $item.addClass(readyClass);
            }

            if (!$item.hasClass(readyClass)) {
                $promocodeRow.prop('hidden', false);
            }
        }
    });

    $promocodeApply.on('click', function(e) {
        e.preventDefault();

        let $button = $(this);

        $bonusPromocode.removeClass(readyClass);
        $promocodeName.prop('hidden', false);
        $promocodeField.prop('hidden', true);
        $promocodeRow.prop('hidden', false);
        $bonusPromocode.addClass(appliedClass);
    });

    $bonusPoints.on('click', function(e) {
        let $item = $(this);

        if (!$item.hasClass(selectedClass)) {
            $item.addClass(selectedClass);
            $bonusPromocode.removeClass(selectedClass).removeClass(readyClass);
            $promocodeControl.prop('checked', false).change();
            $pointsControl.prop('checked', true).change();

            $promocodeRow.prop('hidden', true);
            $pointsRow.prop('hidden', false);
        }
    });

    $pointsApply.on('click', function(e) {
        e.preventDefault();

        let $button = $(this);

        $bonusPoints.addClass(appliedClass);
    });
});
