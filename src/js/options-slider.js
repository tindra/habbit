/* Options slider */
$(document).ready(function(){
    let $optionsSlider = $('[data-slider="options"]');

    $optionsSlider.slick({
        infinite: false,
        slidesToShow: 5,
        swipeToSlide: true,
        responsive: [
            {
                breakpoint: bpXL,
                settings: {
                    slidesToShow: 1,
                    variableWidth: true,
                    autoSlidesToShow: true,
                    arrows: false
                }
            },
        ]
    });

    $(window).on('orientationchange resize', function() {
        $optionsSlider.slick('resize');
    });

});