/* Image slider */
$(document).ready(function(){
    let $imageSlider = $('[data-slider="image"]');

    $imageSlider.slick({
        dots: true,
        responsive: [
            {
                breakpoint: bpLG,
                settings: {
                    arrows: false
                }
            },
        ]
    });

    $(window).on('orientationchange resize', function() {
        $imageSlider.slick('resize');
    });
});