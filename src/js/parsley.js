// Validation errors messages for Parsley
// Load this after Parsley

Parsley.addMessages('en', {
    type: {
        email:        "You entered an invalid email",
    },
});

Parsley.setLocale('en');

/* Forms */
$(document).ready(function() {
    /* Select2 For parsley validation */
    $('[data-element="select"]').change(function() {
        $(this).trigger('input');
        $(this).parsley().validate();
    });

    /* Form Parsley Validation */
    let $parsleyForm = $('[data-parsley-validate]');
    let $parsleyFormSubmit = $parsleyForm.find('[type="submit"]');

    $parsleyForm.parsley({
        excluded: "[disabled], :hidden",
        errorClass: 'has-error',
        successClass: 'has-success',
        errorsWrapper: '<div class="form__errors-list"></div>',
        errorTemplate: '<div class="form__error"></div>',
        errorsContainer (field) {
            return field.$element.parent().closest('.form__group');
        },
        classHandler (field) {
            const $parent = field.$element.closest('.form__group');
            if ($parent.length) return $parent;

            return $parent;
        }
    });

    let checkValid = function(ParsleyForm){
        let submitButton = ParsleyForm.find('[type="submit"]');

        if ( ParsleyForm.parsley().isValid() ) {
            submitButton.prop('disabled', false);
        } else {
            submitButton.prop('disabled', true);
        }
    }

    $parsleyForm.on('parsley:form:success', function(ParsleyForm) {
        ParsleyForm.$element.find('input[type="submit"]').prop('disabled', false);
    });

    $parsleyForm.on('parsley:form:error', function(ParsleyForm) {
        ParsleyForm.$element.find('input[type="submit"]').prop('disabled', true);
    });

    $('[data-parsley-validate] :input').on('input', function() {
        let ParsleyForm =  $(this).closest('[data-parsley-validate]');
        checkValid(ParsleyForm);
    });

    $('[data-form="request-form"]').on('submit', function(e) {
        e.preventDefault();

        const $form = $(this);
    });

});