/* Categories slider */
$(document).ready(function(){
    let $categoriesSlider = $('[data-slider="categories"]');

    $categoriesSlider.slick({
        infinite: false,
        arrows: false,
        slidesToShow: 5,
        focusOnSelect: true,
        swipeToSlide: true,
        responsive: [
            {
                breakpoint: bpLG,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: bpMD,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: bpSM,
                settings: {
                    slidesToShow: 2,
                }
            },
        ]
    });

     let activeCategoryIndex = $categoriesSlider.find('.is-active').closest('.slick-slide').attr('data-slick-index');
     $categoriesSlider.slick('slickGoTo', activeCategoryIndex);

    $(window).on('orientationchange resize', function() {
        $categoriesSlider.slick('resize');
    });
});