/* Testimonials slider */
$(document).ready(function(){
    let $testimonialsSlider = $('[data-slider="testimonials"]');

    $testimonialsSlider.slick({
        dots: true,
        autoplay: true,
        autoplaySpeed: 5000,
        responsive: [
            {
                breakpoint: bpXL,
                settings: {
                    arrows: false,
                }
            },
        ]
    });

    $(window).on('orientationchange resize', function() {
        $testimonialsSlider.slick('resize');
    });
});