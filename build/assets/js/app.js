'use strict';

const activeClass    = 'is-active';
const appliedClass   = 'is-applied';
const fixedClass     = 'is-fixed';
const focusClass     = 'is-focused';
const hoverClass     = 'is-hover';
const disabledClass  = 'is-disabled';
const visibleClass   = 'is-visible';
const invisibleClass = 'is-invisible';
const expandedClass  = 'is-expanded';
const selectedClass  = 'is-selected';
const collapsedClass = 'is-collapsed';
const scrolledClass  = 'is-scrolled';
const readyClass     = 'is-ready';
const headerFixedClass  = 'header-is-fixed';
const lockedScrollClass = 'scroll-is-locked';
const modalVisibleClass      = 'modal-is-visible';
const mobileMenuVisibleClass = 'mobile-menu-is-visible';
const asideMenuVisibleClass  = 'aside-menu-is-visible';

const $header = $('[data-component="header"]');
const $body = $('.body');
const $main = $('.main');

const bpSM = 576;
const bpMD = 768;
const bpLG = 1024;
const bpXL = 1248;
const bp2Xl = 1440;

/* Addresses slider */
$(document).ready(function(){
    let $addressesListSlider = $('[data-slider="addresses-list"]');
    let $addressesSlider = $('[data-slider="addresses"]');

    $addressesListSlider.slick({
        mobileFirst: true,
        infinite: false,
        slidesToShow: 1,
        variableWidth: true,
        autoSlidesToShow: true,
        swipeToSlide: true,
        arrows: false,
        responsive: [
            {
                breakpoint: bpLG - 1,
                settings: "unslick"
            },
        ]
    });

    $addressesSlider.slick({
        infinite: false,
        slidesToShow: 1,
        variableWidth: true,
        autoSlidesToShow: true,
        swipeToSlide: true,
        arrows: false,
    });

    $(window).on('orientationchange resize', function() {
        $addressesListSlider.slick('resize');
        $addressesSlider.slick('resize');
    });
});
// Amount form
$(document).ready(function() {
    $(document).on('click', function(e) {
        var target = $(e.target);
        var amountForm = "";

        if (e.target.dataset.component == "amount-form") {
            amountForm = target;
        } else if (target.parents('[data-component="amount-form"]').length) {
            amountForm = target.parents('[data-component="amount-form"]');
        }

        if (!amountForm) return;

        var
        amountInput = amountForm.find('[data-element="amount-input"]'),
        amountValue = amountInput.val(),
        amountValueNumber = +amountValue,
        amountMin = +amountInput.attr('data-min')  || 0,
        amountMax = +amountInput.attr('data-max'),
        amountStep = +amountInput.attr('data-step') || 1,
        amountNewValue = 0;

        amountInput.focus().val('').val(amountValue);

        if (target.attr('data-element') === 'inc-control' || target.attr('data-element') === 'dec-control') {
            if (target.attr('data-element') === 'inc-control') {
                amountForm.find('[data-element="dec-control"]').prop('disabled', false);
                if (amountMax && amountValueNumber >= amountMax) {
                    amountNewValue = amountValueNumber;
                    target.prop('disabled', true);
                } else {
                    amountNewValue = amountValueNumber + amountStep;
                }
                if (amountNewValue >= amountMax) {
                    target.prop('disabled', true);
                }
            } else if (target.attr('data-element') === 'dec-control') {
                amountForm.find('[data-element="inc-control"]').prop('disabled', false);
                if (amountValueNumber <= amountMin) {
                    amountNewValue = amountValueNumber;
                } else {
                    amountNewValue = amountValueNumber - amountStep;
                }
                if (amountNewValue <= amountMin) {
                    target.prop('disabled', true);
                }
            }
            if (Number.isInteger(amountStep)) {
                amountInput.val(amountNewValue);
            } else {
                amountInput.val(amountNewValue.toFixed(1));
            }
            amountInput.blur();
        } else {
            amountInput.focus().val('').val(amountValue);
        }
    });
});
/* Mobile menu */
let $asideMenu = $('[data-component="aside-menu"]');
let $asideMenuTrigger = $('[data-element="aside-menu-trigger"]');
let $asideMenuClose = $('[data-element="aside-menu-close"]');

function hideasideMenu(component) {
    component.removeClass(visibleClass);
    $body.removeClass(asideMenuVisibleClass);
    $body.removeClass(lockedScrollClass);
}

function showasideMenu(component) {
    component.addClass(visibleClass);
    $body.addClass(lockedScrollClass);
    $body.addClass(asideMenuVisibleClass);
}

$(document).ready(function(){

    $asideMenuTrigger.on('click', function(e) {
        e.preventDefault();

        let $trigger = $(this);
        let triggerSrc = $trigger.attr('data-src');
        let $component = $('[data-component="aside-menu"][data-src="'+ triggerSrc +'"]');

        showasideMenu($component);
    });

    $asideMenuClose.on('click', function(e) {
        e.preventDefault();

        let $closeButton = $(this);
        let $component = $closeButton.closest('[data-component="aside-menu"]');

        hideasideMenu($component);
    });

    $(document).on('keyup', function(e) {
        if (e.key === "Escape" || e.keyCode === 27) {
            hideasideMenu($asideMenu);
        }
    });

    $(document).on('click', function(e) {
        if ($asideMenu.has(e.target).length === 0 && !$asideMenu.is(e.target) && $asideMenuTrigger.has(e.target).length === 0 && !$asideMenuTrigger.is(e.target)) {
            hideasideMenu($asideMenu);
        }
    });
});
/* Catalog slider */
$(document).ready(function(){
    let $catalogSlider = $('[data-slider="catalog"]');
    let $catalogSliderDefault = $('[data-slider="catalog-default"]');

    $catalogSlider.each(function(index, slider) {
        let $slider = $(slider);

        let $prevButton = $slider.closest('.section').find('.section-slider__prev');
        let $nextButton = $slider.closest('.section').find('.section-slider__next');

        $slider.slick({
            infinite: false,
            slidesToShow: 4,
            swipeToSlide: true,
            prevArrow: $prevButton,
            nextArrow: $nextButton,
            responsive: [
                {
                    breakpoint: bpXL,
                    settings: {
                        slidesToShow: 1,
                        variableWidth: true,
                        autoSlidesToShow: true,
                        dots: true
                    }
                },
                {
                    breakpoint: bpLG,
                    settings: 'unslick'
                },
            ]
        });

        $(window).on('orientationchange resize', function() {
            $slider.slick('resize');
        });
    });

    $catalogSliderDefault.each(function(index, slider) {
        let $slider = $(slider);

        let $prevButton = $slider.closest('.section').find('.section-slider__prev');
        let $nextButton = $slider.closest('.section').find('.section-slider__next');

        $slider.slick({
            infinite: false,
            slidesToShow: 4,
            prevArrow: $prevButton,
            nextArrow: $nextButton,
            swipeToSlide: true,
            responsive: [
                {
                    breakpoint: bpXL,
                    settings: {
                        slidesToShow: 3,
                        dots: true
                    }
                },
                {
                    breakpoint: bpLG,
                    settings: {
                        slidesToShow: 2,
                        variableWidth: true,
                        autoSlidesToShow: true,
                        dots: true
                    }
                },
            ]
        });

        $(window).on('orientationchange resize', function() {
            $slider.slick('resize');
        });
    });

});
/* Categories slider */
$(document).ready(function(){
    let $categoriesSlider = $('[data-slider="categories"]');

    $categoriesSlider.slick({
        infinite: false,
        arrows: false,
        slidesToShow: 5,
        focusOnSelect: true,
        swipeToSlide: true,
        responsive: [
            {
                breakpoint: bpLG,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: bpMD,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: bpSM,
                settings: {
                    slidesToShow: 2,
                }
            },
        ]
    });

     let activeCategoryIndex = $categoriesSlider.find('.is-active').closest('.slick-slide').attr('data-slick-index');
     $categoriesSlider.slick('slickGoTo', activeCategoryIndex);

    $(window).on('orientationchange resize', function() {
        $categoriesSlider.slick('resize');
    });
});
/* Categories on scroll */
document.addEventListener('DOMContentLoaded', function() {
    const $categories = document.querySelector('[data-component="categories"]');

    if (!$categories) {
        return false;
    }
    const $navbar = document.querySelector('[data-component="navbar"]');
    const $categoriesLinks = $categories.querySelectorAll('a[href^="#"][data-scroll]');
    const mainOffset = parseInt(window.getComputedStyle($main[0], null).getPropertyValue('padding-top')) - 40;

    let onScroll = function () {
        $categories.classList.toggle(fixedClass, window.scrollY > 0);
    };

    let scrollSpy = function () {
        if ($categoriesLinks.length === 0) {
            return false;
        }

        let scrollPos = (window.scrollY || document.documentElement.scrollTop) + mainOffset;

        Array.from($categoriesLinks).forEach((link) => {
            const $elem = document.querySelector(link.getAttribute('href'));

            if (!$elem) {
                return false;
            }

            return $elem.offsetTop <= scrollPos && ($elem.offsetTop + $elem.clientHeight) > scrollPos
            ? link.classList.add(activeClass)
            : link.classList.remove(activeClass);
        });

        if (scrollPos === mainOffset) {
            $categories.querySelector('a.'+activeClass+'[href^="#"][data-scroll]').classList.remove(activeClass)
            $categoriesLinks[0].classList.add(activeClass)
        }
    };

    onScroll();
    scrollSpy();
    window.addEventListener('scroll', function() {
        onScroll();
        scrollSpy();
    });
});
/* Promocode in checkout */
$(document).ready(function(){
    let $bonusControl     = $('[data-component="bonus"] input[name="bonus"]:radio');
    let $bonusPromocode   = $('[data-element="bonus-promocode"]');
    let $bonusPoints      = $('[data-element="bonus-points"]');
    let $promocodeControl = $('[data-element="promocode-control"]');
    let $promocodeTag     = $('[data-element="promocode-tag"]');
    let $promocodeName    = $('[data-element="promocode-name"]');
    let $promocodeField   = $('[data-element="promocode-field"]');
    let $promocodeApply   = $('[data-element="promocode-apply"]');
    let $promocodeRow     = $('[data-element="promocode-row"]');
    let $pointsControl    = $('[data-element="points-control"]');
    let $pointsRow        = $('[data-element="points-row"]');
    let $pointsData       = $('[data-element="points-data"]');
    let $pointsApply      = $('[data-element="points-apply"]');

    $promocodeTag.on('click', function(e) {
        e.preventDefault();

        let $tag = $(this);

        $promocodeControl.prop('checked', false).change();
        $bonusPromocode.removeClass(selectedClass).addClass(readyClass);
        $promocodeRow.prop('hidden', true);
        $promocodeName.prop('hidden', true);
        $promocodeField.prop('hidden', false);
    });

    $bonusPromocode.on('click', function(e) {
        let $item = $(this);

        if (!$item.hasClass(selectedClass)) {
            $item.addClass(selectedClass);
            $bonusPoints.removeClass(selectedClass);
            $promocodeControl.prop('checked', true).change();
            $pointsControl.prop('checked', false).change();

            $pointsRow.prop('hidden', true);

            if (!$promocodeField.prop('hidden')) {
                $item.addClass(readyClass);
            }

            if (!$item.hasClass(readyClass)) {
                $promocodeRow.prop('hidden', false);
            }
        }
    });

    $promocodeApply.on('click', function(e) {
        e.preventDefault();

        let $button = $(this);

        $bonusPromocode.removeClass(readyClass);
        $promocodeName.prop('hidden', false);
        $promocodeField.prop('hidden', true);
        $promocodeRow.prop('hidden', false);
        $bonusPromocode.addClass(appliedClass);
    });

    $bonusPoints.on('click', function(e) {
        let $item = $(this);

        if (!$item.hasClass(selectedClass)) {
            $item.addClass(selectedClass);
            $bonusPromocode.removeClass(selectedClass).removeClass(readyClass);
            $promocodeControl.prop('checked', false).change();
            $pointsControl.prop('checked', true).change();

            $promocodeRow.prop('hidden', true);
            $pointsRow.prop('hidden', false);
        }
    });

    $pointsApply.on('click', function(e) {
        e.preventDefault();

        let $button = $(this);

        $bonusPoints.addClass(appliedClass);
    });
});

/* Cleave inputs */
(function() {
    let $codeInputs = document.querySelectorAll('[data-element="code-mask"]');

    if ($codeInputs.length !== 0) {
        for (const inputCode of $codeInputs) {
            let codeCleave = new Cleave(inputCode, {
                numericOnly: true,
                blocks: [5],
            });
        }
    }

    let $phoneInputs = document.querySelectorAll('[data-element="phone-mask"]');

    if ($phoneInputs.length !== 0) {
        for (const inputPhone of $phoneInputs) {
            let phoneCleave = new Cleave(inputPhone, {
                delimiters: ['+91 ', ' ', ' ', ' ', ' '],
                blocks: [0, 3, 3, 2, 2],
                numericOnly: true
            });
        }
    }
})();
/* Dropdown */

$(document).ready(function(){
    $('[data-element="dropdown-toggle"]').on('click', function(e){
        e.preventDefault();
    });

    $('[data-type="default"] [data-element="dropdown-toggle"]').each(function () {
        let tippyDropdown = tippy(this, {
            content(reference) {
                const id = reference.getAttribute('data-template');
                const template = document.getElementById(id);
                return template.innerHTML;
            },
            allowHTML: true,
            arrow: false,
            theme: 'light',
            trigger: 'click',
            placement: 'bottom-start',
            interactive: true,
            appendTo: document.body,
            popperOptions: {
                positionFixed: true,
            },
            zIndex: 99,
            offset: [0, 4],
            onShown: function(instance) {
                let $dropdownOption = $(instance.popper).find('[data-element="dropdown-option"]');
                $dropdownOption.on('click', function(e){
                    e.preventDefault();

                    let $option = $(this);
                    let optionText = $option.html();
                    let optionsParentId = $option.closest('[data-element="dropdown-content"]').attr('data-id');
                    let $dropdown = $('[data-template="'+optionsParentId+'"]').closest('[data-component="dropdown"]');
                    let $toggleText = $dropdown.find('[data-element="dropdown-toggle-text"]');

                    $option.siblings().removeClass(selectedClass);
                    $option.addClass(selectedClass);
                    $toggleText.html(optionText);

                    instance.hide();
                });
            }
        });
    });

    $('[data-type="multiple"] [data-element="dropdown-toggle"]').each(function () {
        let tippyDropdown = tippy(this, {
            content(reference) {
                const id = reference.getAttribute('data-template');
                const template = document.getElementById(id);
                return template.innerHTML;
            },
            allowHTML: true,
            arrow: false,
            theme: 'light',
            trigger: 'click',
            placement: 'bottom-start',
            interactive: true,
            appendTo: 'parent',
            onShown: function(instance) {
                let $dropdownCheckbox = $(instance.popper).find('[data-element="dropdown-checkbox"]');
                let $dropdownCheckboxAll = $(instance.popper).find('[data-element="dropdown-checkbox-all"]');

                $dropdownCheckboxAll.on('change', function(e){

                    let $checkboxAll = $(this);
                    let $dropdown = $checkboxAll.closest('[data-component="dropdown"]');
                    let $toggleAmount = $dropdown.find('[data-element="dropdown-toggle-amount"]');
                    let checkedItems = $dropdown.find('input[type=checkbox]:checked:not([data-element="dropdown-checkbox-all"])');

                    if ($checkboxAll.prop('checked')) {
                        checkedItems.prop('checked', false).change();
                        $toggleAmount.html('');
                    }
                });

                $dropdownCheckbox.on('change', function(e){

                    let $checkbox = $(this);
                    let $dropdown = $checkbox.closest('[data-component="dropdown"]');
                    let $checkboxAll = $dropdown.find('[data-element="dropdown-checkbox-all"]');
                    let $toggleAmount = $dropdown.find('[data-element="dropdown-toggle-amount"]');
                    let toggleAmountValue = +$toggleAmount.html();
                    let checkedItems = $dropdown.find('[data-element="dropdown-checkbox"]:checked').length;

                    if (checkedItems > 0) {
                        $checkboxAll.prop('checked', false).change();
                        $toggleAmount.html(checkedItems);
                    } else {
                        $checkboxAll.prop('checked', true).change();
                        $toggleAmount.html('');
                    }
                });
            }
        });
    });
});
/* Float label */
document.addEventListener('DOMContentLoaded', function() {
    const formInputs = document.querySelectorAll('[data-element="floated-field"] .form__input');

    formInputs.forEach(input => {

        input.addEventListener("focus", () => {
            const formGroup = input.closest('.form__field');

            formGroup.classList.add(focusClass); 
        });

        input.addEventListener("blur", () => {
            let inputValue = input.value;

            if (inputValue == '' ) {
                const formGroup = input.closest('.form__field');
                formGroup.classList.remove(focusClass);
            }
        });
    });
});
/* Hero slider */
$(document).ready(function(){
    let $heroSlider = $('[data-slider="hero"]');

    $heroSlider.slick({
        dots: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 5000,
    });

    heroContentAdjustment();
    $(window).on('resize orientationchange', function() {
        $heroSlider.slick('resize');
        heroContentAdjustment();
    });

    function heroContentAdjustment() {
        let $heroContent = $('[data-element="hero-content"]');

        $heroContent.css('height', '');

        let tallestContent = Math.max.apply(Math, $heroContent.map(function(index, el) {
            return $(el)[0].scrollHeight;
        }));

        if ($(window).width() < bpLG) {
            $heroContent.css('height', tallestContent + 'px');
        } else {
            $heroContent.css('height', '');
        }
    };
});
/* Image slider */
$(document).ready(function(){
    let $imageSlider = $('[data-slider="image"]');

    $imageSlider.slick({
        dots: true,
        responsive: [
            {
                breakpoint: bpLG,
                settings: {
                    arrows: false
                }
            },
        ]
    });

    $(window).on('orientationchange resize', function() {
        $imageSlider.slick('resize');
    });
});
/* Items slider */
$(document).ready(function(){
    let $itemsSlider = $('[data-slider="items"]');

    $itemsSlider.each(function(index, slider) {
        let $slider = $(slider);

        let $prevButton = $slider.closest('.section').find('.section-slider__prev');
        let $nextButton = $slider.closest('.section').find('.section-slider__next');

        $slider.slick({
            infinite: false,
            slidesToShow: 4,
            prevArrow: $prevButton,
            nextArrow: $nextButton,
            swipeToSlide: true,
            responsive: [
                {
                    breakpoint: bpXL,
                    settings: {
                        slidesToShow: 1,
                        variableWidth: true,
                        autoSlidesToShow: true,
                        dots: true
                    }
                },
                {
                    breakpoint: bpMD,
                    settings: {
                        slidesToShow: 1,
                        variableWidth: true,
                        autoSlidesToShow: true,
                        dots: true
                    }
                },
            ]
        });

        $(window).on('orientationchange resize', function() {
            $slider.slick('resize');
        });
    });

});
const observer = lozad(); // lazy loads elements with default selector as '.lozad'
observer.observe();
/* Mobile menu */
let $mobileMenu = $('[data-component="mobile-menu"]');
let $mobileMenuTrigger = $('[data-element="mobile-menu-trigger"]');
let $mobileMenuToggle = $('[data-element="mobile-menu-toggle"]');
let $mobileMenuClose = $('[data-element="mobile-menu-close"]');

function hideMobileMenu(component) {
    component.removeClass(visibleClass);

    let parentMenu = component.parents('[data-component="mobile-menu"]');

    if (parentMenu.length === 0) {
        $body.removeClass(mobileMenuVisibleClass);
        $body.removeClass(lockedScrollClass);
    }
}

function showMobileMenu(component) {
    if ($(window).width() < bpXL) {
        component.addClass(visibleClass);
        $body.addClass(lockedScrollClass);
        $body.addClass(mobileMenuVisibleClass);
    }
}

function desktopCheck() {
    if ($(window).width() >= bpXL) {
        hideMobileMenu($mobileMenu);
    }
}

$(document).ready(function(){

    $mobileMenuToggle.on('click', function(e) {
        e.preventDefault();

        let $toggle = $(this);
        let toggleSrc = $toggle.attr('data-src');
        let $component = $('[data-component="mobile-menu"][data-src="'+ toggleSrc +'"]');

        if ($component.hasClass(visibleClass)) {
            hideMobileMenu($component);
            $('[data-component="mobile-menu"]').removeClass(visibleClass);
        } else {
            $('[data-component="mobile-menu"]').not($component).removeClass(visibleClass);
            showMobileMenu($component);
        }
    });

    $mobileMenuTrigger.on('click', function(e) {
        e.preventDefault();

        let $trigger = $(this);
        let triggerSrc = $trigger.attr('data-src');
        let $component = $('[data-component="mobile-menu"][data-src="'+ triggerSrc +'"]');

        showMobileMenu($component);
    });

    $mobileMenuClose.on('click', function(e) {
        e.preventDefault();

        let $closeButton = $(this);
        let $component = $closeButton.closest('[data-component="mobile-menu"]');

        hideMobileMenu($component);
    });

    $(document).on('keyup', function(e) {
        if ($(window).width() < bpXL) {
            if (e.key === "Escape" || e.keyCode === 27) {
                hideMobileMenu($mobileMenu);
            }
        }
    });

    $(document).on('click', function(e) {
        if ($(window).width() < bpXL) {
            if ($mobileMenu.has(e.target).length === 0 && !$mobileMenu.is(e.target) && $mobileMenuTrigger.has(e.target).length === 0 && !$mobileMenuTrigger.is(e.target) && $mobileMenuToggle.has(e.target).length === 0 && !$mobileMenuToggle.is(e.target)) {
                hideMobileMenu($mobileMenu);
            }
        }
    });

    desktopCheck();

    $(window).on('orientationchange resize', function() {
        desktopCheck();
    });
});
/* Modal */
$(function() {
    $('[data-element="modal-trigger"]').magnificPopup({
        type: 'inline',
        fixedContentPos: true,
        fixedBgPos: true,

        overflowY: 'auto',

        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        callbacks: {
            beforeOpen: function() {
                let effectData = this.st.el.attr('data-effect');

                if (!effectData) {
                    effectData = 'mfp-zoom-in';
                }

                this.st.mainClass = effectData;
            },
            open: function() {
                $('.body').css('overflow', "hidden").addClass(modalVisibleClass);
                let $mfp = $.magnificPopup.instance;
                let $trigger = $($mfp.currItem.el[0]);
                $trigger.attr('disabled', true);
            },
            close: function() {
                $('.body').css('overflow', "").removeClass(modalVisibleClass);
                let $mfp = $.magnificPopup.instance;
                let $trigger = $($mfp.currItem.el[0]);
                $trigger.removeAttr('disabled');
            }
        },
    });


    /* Gallery Modal
    $('[data-element="gallery-trigger"]').magnificPopup({
        type: 'image',
        closeOnContentClick: false,
        fixedContentPos: false,
        mainClass: 'mfp-zoom-in',
        image: {
            verticalFit: true,
        },
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300, // don't foget to change the duration also in CSS
            opener: function(element) {
                return element.find('img');
            }
        },
        callbacks: {
            buildControls: function() {
                // re-appends controls inside the main container
                this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
            }
        }
    }); */

    /* Modal with video */
    $('[data-element="modal-video-trigger"]').magnificPopup({
        type: 'iframe',
        mainClass: 'mfp-zoom-in',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false,
    });

    $(document).on('click', '[data-element="modal-close"]', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });
});

/* Move a block between it's mobile and desktop placeholders  */
$(document).ready(function(){
    moveBlocks();
    $(window).on('orientationchange resize', function() {
        moveBlocks();
    });

    function moveBlocks() {
        let $movableBlock = $('[data-block="movable-block"]');

        $movableBlock.each(function (index, item) {
            let $block = $(item);
            let blockID = $block.attr('data-id');
            let $blockPlaceholderMobile = $('[data-element="placeholder"][data-device="mobile"][data-id="' + blockID + '"]');
            let $blockPlaceholderTablet = $('[data-element="placeholder"][data-device="tablet"][data-id="' + blockID + '"]');
            let $blockPlaceholderDesktop = $('[data-element="placeholder"][data-device="desktop"][data-id="' + blockID + '"]');
            let blockBreakpoint = $block.attr('data-breakpoint');
            let windowWidth = $(window).width();
            let breakpoint = bpLG;

            if (blockBreakpoint) {
                breakpoint = blockBreakpoint;
            }

            if (windowWidth >= breakpoint) {
                if ($blockPlaceholderDesktop.length === 0) {
                    $block.appendTo($blockPlaceholderTablet);
                } else {
                    $block.appendTo($blockPlaceholderDesktop);
                }
            } else if (windowWidth >= bpMD && windowWidth < bpLG && $blockPlaceholderTablet.length > 0) {
                $block.appendTo($blockPlaceholderTablet);
            } else if (windowWidth < bpMD) {
                $block.appendTo($blockPlaceholderMobile);
            }
        });
    }
});

/* Panel on mobile, tabs on desktop */
$(document).ready(function(){
    responsivePanelTabs();

    $(window).on('orientationchange resize', function() {
        responsivePanelTabs();
    });

    function responsivePanelTabs() {
        let $panelTabs = $('[data-component="panel-tabs"]');
        let $panelTabsToggle = $('[data-element="panel-tabs-toggle"]');
        let $panelTabsContent = $('[data-element="panel-tabs-content"]');

        if ($(window).width() < bpXL) {
            $panelTabs.each(function (index, item) {
                let $component = $(item);
                let $tab = $component.find('[data-element="panel-tabs-tab"]');

                $tab.hide().removeClass(activeClass);
                $component.find('[data-element="panel-tabs-toggle"]').removeClass(activeClass);
                $component.find('[data-element="panel-tabs-item"]').addClass(collapsedClass);

                $tab.each(function (index, item) {
                    let $tab = $(item);
                    let tabID = $tab.attr('data-id');
                    let $toggle = $component.find('[data-element="panel-tabs-toggle"][data-id="'+tabID+'"]');
                    let $item = $toggle.closest('[data-element="panel-tabs-item"][data-id="'+tabID+'"]');

                    $toggle.after($tab);
                });
            });

            $panelTabs.on('click', '[data-element="panel-tabs-toggle"]:not([data-link="normal"])', function(e) {
                e.preventDefault();

                let $toggle = $(e.currentTarget);
                let $item   = $toggle.closest('[data-element="panel-tabs-item"]');
                let $tab    = $item.find('[data-element="panel-tabs-tab"]');

                if ($item.hasClass(collapsedClass)) {
                    $tab.slideDown(250);
                    $item.removeClass(collapsedClass);
                } else {
                    $tab.slideUp(250);
                    $item.addClass(collapsedClass);
                }
            });
        } else {
            $panelTabs.each(function (index, item) {
                let $component = $(item);
                let $tab = $component.find('[data-element="panel-tabs-tab"]');
                let $content = $component.find('[data-element="panel-tabs-content"]');

                $tab.attr('style', '');
                $component.find('[data-element="panel-tabs-item"]').removeClass(collapsedClass);

                $tab.appendTo($content);
            });

            $panelTabsToggle.on('mouseenter', function(e) {
                let $toggle = $(this);
                let toggleID = $toggle.attr('data-id');
                let $tabActive = '[data-element="panel-tabs-tab"][data-id="' + toggleID +'"]';
                let $component = $toggle.closest('[data-component="panel-tabs"]');
                let $defaultImg = $component.find('[data-element="panel-tabs-default-img"]');

                console.log($defaultImg);

                if (!($toggle.hasClass(activeClass))) {
                    $defaultImg.removeClass(activeClass);
                    $component.find('[data-element="panel-tabs-tab"]').removeClass(activeClass);
                    $component.find('[data-element="panel-tabs-toggle"]').removeClass(activeClass);
                    $component.find($tabActive).addClass(activeClass);
                    $toggle.addClass(activeClass);
                }
            });

            $panelTabsContent.on('mouseleave', function(e) {
                let $component = $(this).closest('[data-component="panel-tabs"]');
                let $defaultImg = $component.find('[data-element="panel-tabs-default-img"]');

                $component.find('[data-element="panel-tabs-tab"]').removeClass(activeClass);
                $component.find('[data-element="panel-tabs-toggle"]').removeClass(activeClass);
                $defaultImg.addClass(activeClass);
            });
        }
    }
});
/* Options slider */
$(document).ready(function(){
    let $optionsSlider = $('[data-slider="options"]');

    $optionsSlider.slick({
        infinite: false,
        slidesToShow: 5,
        swipeToSlide: true,
        responsive: [
            {
                breakpoint: bpXL,
                settings: {
                    slidesToShow: 1,
                    variableWidth: true,
                    autoSlidesToShow: true,
                    arrows: false
                }
            },
        ]
    });

    $(window).on('orientationchange resize', function() {
        $optionsSlider.slick('resize');
    });

});
/* Options */
$(document).ready(function(){
    $('[data-element="options-item"]').on('click', function(e){
        let $option = $(this);
        let $component = $option.closest('[data-component="options"]');
        let $data = $component.find('[data-element="options-data"]');
        let $product = $('[data-component="product"]');
        let optionTitle = $option.attr('title');

        $data.html(optionTitle);

        if ($option.hasClass(disabledClass)) {
            $product.addClass(disabledClass);
        } else {
            $product.removeClass(disabledClass);
        }
    });
});
/* Panel */
$(document).ready(function(){
    let $panelToggle = $('[data-element="panel-toggle"]');

    $panelToggle.on('click', function(e){
        let $toggle  = $(this);
        let toggleDisabledOn = $toggle.attr('data-disabled-on');
        let windowWidth = $(window).width();

        if ((toggleDisabledOn == 'tablet' && windowWidth >= bpMD) || (toggleDisabledOn == 'desktop' && windowWidth >= bpXL)) {
            if ($toggle.attr('href')) {
                window.location = this.href;
            } else {
                e.preventDefault();
            }
        } else {
            e.preventDefault();

            let $panel   = $toggle.closest('[data-component="panel"]');
            let $content = $panel.find('[data-element="panel-content"]');

            if ($panel.hasClass(collapsedClass)) {
                $content.slideDown(250);
                $panel.removeClass(collapsedClass);
            } else {
                $content.slideUp(250);
                $panel.addClass(collapsedClass);
            }
        }
    });
});
// Validation errors messages for Parsley
// Load this after Parsley

Parsley.addMessages('en', {
    type: {
        email:        "You entered an invalid email",
    },
});

Parsley.setLocale('en');

/* Forms */
$(document).ready(function() {
    /* Select2 For parsley validation */
    $('[data-element="select"]').change(function() {
        $(this).trigger('input');
        $(this).parsley().validate();
    });

    /* Form Parsley Validation */
    let $parsleyForm = $('[data-parsley-validate]');
    let $parsleyFormSubmit = $parsleyForm.find('[type="submit"]');

    $parsleyForm.parsley({
        excluded: "[disabled], :hidden",
        errorClass: 'has-error',
        successClass: 'has-success',
        errorsWrapper: '<div class="form__errors-list"></div>',
        errorTemplate: '<div class="form__error"></div>',
        errorsContainer (field) {
            return field.$element.parent().closest('.form__group');
        },
        classHandler (field) {
            const $parent = field.$element.closest('.form__group');
            if ($parent.length) return $parent;

            return $parent;
        }
    });

    let checkValid = function(ParsleyForm){
        let submitButton = ParsleyForm.find('[type="submit"]');

        if ( ParsleyForm.parsley().isValid() ) {
            submitButton.prop('disabled', false);
        } else {
            submitButton.prop('disabled', true);
        }
    }

    $parsleyForm.on('parsley:form:success', function(ParsleyForm) {
        ParsleyForm.$element.find('input[type="submit"]').prop('disabled', false);
    });

    $parsleyForm.on('parsley:form:error', function(ParsleyForm) {
        ParsleyForm.$element.find('input[type="submit"]').prop('disabled', true);
    });

    $('[data-parsley-validate] :input').on('input', function() {
        let ParsleyForm =  $(this).closest('[data-parsley-validate]');
        checkValid(ParsleyForm);
    });

    $('[data-form="request-form"]').on('submit', function(e) {
        e.preventDefault();

        const $form = $(this);
    });

});
/* Partners slider */
$(document).ready(function(){
    let $partnersSlider = $('[data-slider="partners"]');

    $partnersSlider.slick({
        infinite: false,
        slidesToShow: 4,
        swipeToSlide: true,
        responsive: [
            {
                breakpoint: bpLG,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: bpSM,
                settings: {
                    slidesToShow: 2,
                }
            },
        ]
    });

    $(window).on('orientationchange resize', function() {
        $partnersSlider.slick('resize');
    });
});
/* Product Slider */
$(document).ready(function(){
    let $productSlider = $('[data-slider="product"]');

    $productSlider.slick({
        infinite: false,
        arrows: false,
        asNavFor: '[data-slider="product-thumbs"]',
        responsive: [
            {
                breakpoint: bpLG,
                settings: {
                    dots: true,
                }
            },
        ]
    });

    let $productThumbs = $('[data-slider="product-thumbs"]');
    $productThumbs.slick({
        infinite: false,
        vertical: true,
        verticalSwiping: true,
        slidesToShow: 6,
        slidesToScroll: 1,
        focusOnSelect: true,
        asNavFor: '[data-slider="product"]',
        responsive: [
            {
                breakpoint: bpLG,
                settings: 'unslick'
            },
        ]
    });

    $(window).on('orientationchange resize', function() {
        $productSlider.slick('resize');
        $productThumbs.slick('resize');
    });
});

/* Orderbar on scroll */
document.addEventListener('DOMContentLoaded', function() {
    const $orderbarClone = document.querySelector('[data-component="orderbar-clone"]');

    if (!$orderbarClone) {
        return false;
    }

    const $orderbar = document.querySelector('[data-component="orderbar"]');
    const navbarHeight = document.querySelector('[data-component="navbar"]').offsetHeight;
    const orderbarOffset = $orderbar.getBoundingClientRect().top + $orderbar.offsetHeight - navbarHeight;

    function orderbarOnScroll() {
        let onScroll = function () {
            $orderbarClone.classList.toggle(visibleClass, window.scrollY > orderbarOffset);
        };

        onScroll();
        window.addEventListener('scroll', function() {
            onScroll();
        });
    }

    orderbarOnScroll();
});
/* Review images Slider */
$(document).ready(function(){
    let $reviewImagesSlider = $('[data-slider="review-images"]');

    $reviewImagesSlider.each(function() {
        var $gallery = $(this);

        $gallery
        .slick({
            mobileFirst: true,
            infinite: false,
            arrows: false,
            swipeToSlide: true,
            slidesToShow: 1,
            variableWidth: true,
            autoSlidesToShow: true,
            responsive: [
                {
                    breakpoint: bpLG - 1,
                    settings: 'unslick'
                },
            ]
        })
        .magnificPopup({
            type: 'image',
            closeOnContentClick: false,
            fixedContentPos: false,
            mainClass: 'mfp-zoom-in',
            delegate: '[data-element="gallery-trigger"]:not(.slick-cloned)',
            image: {
                verticalFit: true,
            },
            gallery: {
                enabled: true
            },
            callbacks: {
                open: function() {
                    $('.body').css('overflow', "hidden").addClass(modalVisibleClass);
                    var current = $gallery.slick('slickCurrentSlide');
                    $gallery.magnificPopup('goTo', current);
                },
                close: function() {
                    $('.body').css('overflow', "").removeClass(modalVisibleClass);
                },
                beforeClose: function() {
                    $gallery.slick('slickGoTo', parseInt(this.index));
                },
                buildControls: function() {
                    // re-appends controls inside the main container
                    this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
                }
            }
        });

        reviewsImagesTotal($gallery);
        $(window).on('orientationchange resize', function() {
            $gallery.slick('resize');
            reviewsImagesTotal($gallery);
        });
    });

    function reviewsImagesTotal(slider) {
        if ($(window).width() >= bpLG) {
            let $slider = $(slider)
            let $sliderImages = $slider.find('[data-element="gallery-trigger"]');
            let sliderImagesCount = $sliderImages.length;
            let sliderImagesVisivbleCount = 5;
            let sliderImagesMoreCount = sliderImagesCount - sliderImagesVisivbleCount;

            if (sliderImagesMoreCount > 0) {
                $slider.children('[data-element="gallery-trigger"]').eq(sliderImagesVisivbleCount - 1).attr('data-title', '+'+sliderImagesMoreCount);
            }
        }
    };

});
/* Select2 */
$(document).ready(function() {
    let selectCommonOptions = {
        width: '100%',
    };

    let $select = $('[data-element="select"]');

    if (!$select) {
        return false;
    }

    $select.each(function(index, select) {
        let $selectEl = $(select);

        $selectEl.select2(selectCommonOptions).on('select2:open', function(e){
            $('.select2-search__field').attr('placeholder', $(this).attr('data-search-placeholder'));
        });
    });
});
/* Show more */
$(document).ready(function() {
    let $showMoreComponent = $('[data-component="show-more"]');

    function showMore() {
        $showMoreComponent.each(function(index, component) {
            let $component = $(component);
            let $list = $component.find('[data-element="show-more-list"]');
            let $items = $component.find('[data-element="show-more-item"]');
            let $button = $component.find('[data-element="show-more-button"]');
            let $pagination = $component.find('[data-element="show-more-pagination"]');
            let buttonElement = '';

            if ($(window).width() < bpLG) {
                let itemsLength = $items.length;
                let itemsNumber = 4;
                let loadItemsNumber = itemsNumber;
                let itemsNumberDataset = $component.attr('data-items');
                let buttonCount = $component.attr('data-button-count');

                if (itemsNumberDataset) {
                    itemsNumber = itemsNumberDataset;
                }

                loadItemsNumber = itemsNumber;

                if (buttonCount) {
                    buttonElement = '<div class="pagination" data-element="show-more-pagination"><button class="button button--link button--link-light" data-element="show-more-button">Show <span data-element="show-more-button-count">&nbsp;'+loadItemsNumber+'&nbsp;</span> more</button></div>';
                } else {
                    buttonElement = '<div class="pagination" data-element="show-more-pagination"><button class="button button--link button--link-light" data-element="show-more-button">Show more</button></div>';
                }

                if (itemsLength > itemsNumber) {
                    if ($pagination.length === 0) {
                        $list.after(buttonElement);
                    }

                    $items.slice(loadItemsNumber).prop('hidden', true);
                } else {
                    $pagination.remove();
                }

            } else {
                $items.prop('hidden', false);
                $pagination.remove();
            }

        });
    }

    $(window).on('orientationchange resize load', function() {
        showMore();
    });
});
let scroll = new SmoothScroll('[data-scroll]', {
    speed: 500,
    easing: 'easeInOutCubic',
    header: '[data-component="navbar"]',
    offset: function (anchor, toggle) {
        if (toggle.closest('[data-component="categories"]')) {
            let categoriesHeight = document.querySelector('[data-component="categories"]').offsetHeight;
            return categoriesHeight;
        } else {
            return 100;
        }
    }
});
/* Testimonials slider */
$(document).ready(function(){
    let $testimonialsSlider = $('[data-slider="testimonials"]');

    $testimonialsSlider.slick({
        dots: true,
        autoplay: true,
        autoplaySpeed: 5000,
        responsive: [
            {
                breakpoint: bpXL,
                settings: {
                    arrows: false,
                }
            },
        ]
    });

    $(window).on('orientationchange resize', function() {
        $testimonialsSlider.slick('resize');
    });
});
/* Tippy */
let tooltip = tippy('[data-element="tooltip-trigger"]', {
    placement: 'top-start',
    maxWidth: 153,
    theme: 'light',
    offset: [0, 8],
    arrow: false,
    allowHTML: true,
    zIndex: 99,
    content(reference) {
        let tooltipContent = '<div class="tooltip">'+ reference.dataset.content + '</div>'
        return tooltipContent;
    },
});

let popover = tippy('[data-element="popover-trigger"]', {
    maxWidth: 333,
    theme: 'light',
    offset: [0, 21],
    allowHTML: true,
    interactive: true,
    zIndex: 99,
});

let productPopover = tippy('[data-element="product-popover-trigger"]', {
    maxWidth: 282,
    theme: 'light',
    offset: [0, 5],
    allowHTML: true,
    arrow: false,
    interactive: true,
    appendTo: () => document.body,
    interactive: true,
    zIndex: 99,
    content(reference) {
        const id = reference.getAttribute('data-template');
        const template = document.getElementById(id);
        return template.innerHTML;
    },
});

window.addEventListener('scroll', function() {
    tippy.hideAll();
});
/* Animate on Scroll */
$(document).ready(function(){
    AOS.init({
        duration: 500,
        easing: 'ease-in-out',
    });
});